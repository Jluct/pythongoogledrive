class Observer:
    """
    WORKS!
    """
    __observers = {}
    test = 'Observer'

    def register(self, event, name, func, *args):
        if event in self.__observers and callable(func):
            self.__observers[event][name] = {}
            self.__observers[event][name]['func'] = func
            self.__observers[event][name]['param'] = args
        return False

    def register_event(self, event):
        if event not in self.__observers:
            self.__observers[event] = {}
            return self
        return False

    def unregister(self):
        pass

    def unregister_event(self):
        pass

    def now_event(self, event):
        self.__call_event(self.__observers[event])

    def __call_event(self, event):
        for key in event:
            event[key]['func']() if not event[key]['param'] else event[key]['func'](*event[key]['param'])
