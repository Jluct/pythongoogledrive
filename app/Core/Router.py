import json
import os
from .Helpers.JsonReader import JsonReader


class Router:
    __routing = ''

    def __init__(self, path=os.getcwd() + '/app/settings/route.json'):
        self.__routing = JsonReader.read_file(path)

    def add_action(self, command, controller='default', action='main', callback=''):
        self.__routing[command] = {'controller': controller, 'action': action,
                                   'callback': callback if callable(callback) else ''}

    def get_action(self, command):
        if command in self.__routing:
            return self.__routing[command]
        return False

        # [
        #   {
        #     "command": "list",
        #     "controller": "default",
        #     "action": "list"
        #   }
        # ]
