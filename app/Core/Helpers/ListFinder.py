class ListFinder:
    @staticmethod
    def find_by(search_list, key, value):
        for item in search_list:
            if key not in item:
                continue
            if item[key] is value:
                return item
        return False
