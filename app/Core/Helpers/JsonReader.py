import json
import os.path as p


class JsonReader:
    @staticmethod
    def read_file(path):

        if not p.isfile(path):
            return False
        return json.loads(open(p.normpath(path)).read())
