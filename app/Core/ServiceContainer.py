class ServiceContainer:

    __container = {}

    def add_service(self, key, obj):
        self.__container[key] = obj

    def get_service(self, key):
        if key in self.__container:
            return self.__container[key]
