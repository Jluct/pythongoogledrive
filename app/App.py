from app.Core.ServiceContainer import ServiceContainer
from app.Core.ConsoleParser import ConsoleParser
from app.Core.Router import Router
from app.Core.Observer import Observer
from .Core.Helpers.JsonReader import JsonReader

import importlib

from src.AppModule.Services.RemoteDrive.Drive import Drive


class App:
    __setting = {}
    services = False

    def __init__(self):

        self.__setting = JsonReader.read_file('./app/settings/app.json')
        # print(self.__setting['local']['GoogleDrive'])
        # exit(0)

        self.services = ServiceContainer()
        self.services.add_service('ConsoleParser', ConsoleParser())
        self.services.add_service('Router', Router())
        self.services.add_service('Observer', Observer())
        self.services.add_service('Drive', Drive(self.__setting['defaultDrive'], self.__setting['local']))

        # ob = self.services.get_service('Observer')
        # ob.register_event('TEST')
        # ob.register('TEST', 'test', self.test_func, 'data')
        # ob.register('TEST', 'test1', self.test_func1, 'data', 'data1')
        # ob.register('TEST', 'test2', self.test_func2)
        # ob.now_event('TEST')

    def works(self, str_command=''):

        cp = self.services.get_service('ConsoleParser')
        command = cp.split_command(str_command)
        r = self.services.get_service('Router')
        data = r.get_action(command[0])

        if not data:
            print('Command "{}" not found'.format(command[0]))
            return False

        if 'module' in data:
            module = data['module']
        else:
            module = 'AppModule'

        if 'controller' in data:
            controller = data['controller']
        else:
            controller = 'AppController'

        if 'action' in data:
            action = data['action']
        else:
            action = 'default_action'

        use_controller = importlib.import_module('src.' + module + '.Controllers.' + controller, controller)

        class_inst = getattr(use_controller, controller)
        class_inst = class_inst(self.services, self.__setting)
        use_method = getattr(class_inst, action)

        use_method(*command[1:])

    def get_setting(self):
        return self.__setting
