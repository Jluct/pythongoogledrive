from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from app.Core.Helpers.ListFinder import ListFinder
import os
import mimetypes


class PyGoogleDrive:
    drive = ''
    files_list = ''

    def __init__(self):
        # print('init ' + str(self.__class__))
        gauth = GoogleAuth()
        gauth.LocalWebserverAuth()  # Creates local webserver and auto handles authentication.

        self.drive = GoogleDrive(gauth)
        self.get_file_list()
        # pass

    def get_file_list(self, dir='root'):
        """
        application/vnd.google-apps.folder это папка
        Соответственно тут нужно или её отдельно сканировать
        или добавлять в элемент массива массив с файлами и папками

        Сменить директорию при переходе в другую папку
        :param dir: id from file listing GoogleDrive
        :return:
        """
        print(dir)
        self.files_list = self.drive.ListFile({'q': "'" + dir + "' in parents and trashed=false"}).GetList()
        return self.files_list
        # i = 0
        # for file1 in self.files_list:
        #     i = i + 1
        #     print('number: %d, title: %s, id: %s, mimeType: %s' % (i, file1['title'], file1['id'], file1['mimeType']))

    def download(self, file):
        file = self.drive.CreateFile({'id': file['id'], 'mimeType': file['mimeType']})
        file.GetContentFile(file['title'])
        # вызов обсервера для обновления списка файлов в локале

    def get_info(self, id, listing=True):
        file = self.drive.CreateFile({'id': self.files_list[id]['id']})
        return file.FetchMetadata(listing)

    def upload(self, file):

        if not os.path.isfile(file):
            print(file + " is not a file")
            exit(1)

        mime = mimetypes.guess_type(file)
        print(mime)
        print(file + " " + mime[0])

        # f = open(file, 'rb')

        rd_file = self.drive.CreateFile({'title': file, 'mimeType': mime[0]})
        # rd_file = self.drive.CreateFile({'title': file, 'mimeType': mime[0]})
        # rd_file.SetContentString(f.read())  # А если архив и прочее рекомендуют SetContentFile(file_name).

        rd_file.SetContentFile(file)  # проблемы с rar. Google перестал его выгружать, но метод выкачивает. Не верный тип?

        # f.close()
        rd_file.Upload()

        # вызов обсервера для обновления списка файлов на удалённом диске
        return True

    def update(self):
        pass
