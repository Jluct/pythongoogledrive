from ..GoogleDrive.PyGoogleDrive import PyGoogleDrive
from os import path, chdir, mkdir, listdir, getcwd


class Drive:
    __drive = {}
    __use_drive = ''
    setting = ''
    local_file = {}

    def __init__(self, drive, setting):
        self.setting = setting
        self.init_drive(drive)

    def init_drive(self, drive):
        self.__drive[drive] = globals()[drive]()
        self.set_use_drive(drive)
        self.local_list_file()
        # print(drive + " - " + str(self.__drive[drive]))

    def set_use_drive(self, drive):
        self.__use_drive = drive
        disk_path = self.setting[self.__use_drive]['path']
        if not path.isdir(disk_path):
            mkdir(disk_path)

        chdir(disk_path)

    def remote_list_file(self, dir):
        """
        :param dir: string name dir
        :return: PyGoogleDrive object
        """
        return self.__drive[self.__use_drive].get_file_list(dir)

    def download_file(self, file):
        # print("download {}".format(self.__drive[self.__use_drive].files_list[number]))
        self.__drive[self.__use_drive].download(file)

    def local_list_file(self, dir=''):
        """
        Необходимо менять директорию при просмотре файлов в каталоге
        :param dir: name dir
        :return: list files
        """
        # drive = self.__drive[self.__use_drive]
        dir_path = path.normpath(self.setting[self.__use_drive]['path'] + dir)
        self.local_file[self.__use_drive] = listdir(dir_path)

        return self.local_file[self.__use_drive]

    def exist_file(self, title):
        for item in self.setting[self.__use_drive].files_list:
            if title == item['title']:
                return True
        return False

    def get_file(self, key, value):
        drive = self.__drive[self.__use_drive]

        if key not in drive.files_list:
            return False

        for item in drive.files_list:
            if value == item[key]:
                return item

        return False

    def upload_file(self, file):
        """
        Сохранение файла проще вынести в команду

        """
        self.__drive[self.__use_drive].upload(file)

    def update_file(self, file):
        self.__drive[self.__use_drive].upload(file)

    def get_file_list(self):
        return self.__drive[self.__use_drive].files_list

    def get_use_drive(self):
        return self.__use_drive
