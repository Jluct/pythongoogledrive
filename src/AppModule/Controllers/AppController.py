from app.Core.Controller import Controller
from app.Core.Helpers.ListFinder import ListFinder
import pprint


class AppController(Controller):
    def __init__(self, service, setting):
        super().__init__(service, setting)

    def activate_action(self, drive_name='PyGoogleDrive'):
        # print('-- init test action --')
        drive = self.service.get_service('Drive')
        drive.init_drive(drive_name)

    def remote_list_action(self, dir='root'):
        """
        :param dir: integer number dir for listing files || string root
        :return: void
        """
        drive = self.service.get_service('Drive')

        if dir != 'root':
            dir = drive.get_file_list()[int(dir) - 1]['id']

        files = drive.remote_list_file(dir)

        i = 0
        for file1 in files:
            i = i + 1
            print('number: %d, title: %s, id: %s, mimeType: %s' % (i, file1['title'], file1['id'], file1['mimeType']))

    def local_list_action(self, dir=''):
        drive = self.service.get_service('Drive')
        files = drive.local_list_file(dir)  # А вот тут представление добавим

        i = 0
        for item in files:
            i = i + 1
            print("{}. title => {}".format(i, item))  # local
            #     print("{}. title => {}, is remote => {}".format(i, item, str(self.exist_file(item))))  # remote

    def download_file_action(self, number):
        self.service.get_service('Drive').download_file(
            self.service.get_service('Drive').get_file_list()[int(number) - 1])

    def upload_file_action(self, number):

        drive = self.service.get_service('Drive')
        file = drive.local_list_file()[int(number) - 1]
        drive.upload_file(file)

    def default_action(self, test):
        # print('init default action. Param:' + test)
        pass

    def description_file(self, number=None):
        file = self.service.get_service('Drive').get_file_list()[int(number) - 1]  # remote

        # local
        # file = {'embedLink': 'https://drive.google.com/file/d/0BySf-c-YW0YAcUJ4SnhlYm5reFU/preview?usp=drivesdk',
        #         'owners': [{'isAuthenticatedUser': True, 'permissionId': '17510777207640926192', 'kind': 'drive#user',
        #                     'emailAddress': 'panaev02@gmail.com', 'displayName': 'Сергей Панаев'}],
        #         'selfLink': 'https://www.googleapis.com/drive/v2/files/0BySf-c-YW0YAcUJ4SnhlYm5reFU', 'editable': True,
        #         'version': '14686', 'parents': [
        #         {'parentLink': 'https://www.googleapis.com/drive/v2/files/0ACSf-c-YW0YAUk9PVA', 'isRoot': True,
        #          'id': '0ACSf-c-YW0YAUk9PVA',
        #          'selfLink': 'https://www.googleapis.com/drive/v2/files/0BySf-c-YW0YAcUJ4SnhlYm5reFU/parents/0ACSf-c-YW0YAUk9PVA',
        #          'kind': 'drive#parentReference'}], 'fileSize': '128335', 'quotaBytesUsed': '128335',
        #         'headRevisionId': '0BySf-c-YW0YAVWoyeFJLQkRTTHJPQVBodnBCRldTTVZBcTRjPQ',
        #         'originalFilename': 'project.rar', 'lastModifyingUserName': 'Сергей Панаев', 'writersCanShare': True,
        #         'markedViewedByMeDate': '1970-01-01T00:00:00.000Z', 'id': '0BySf-c-YW0YAcUJ4SnhlYm5reFU',
        #         'createdDate': '2017-06-16T10:41:55.046Z', 'title': 'project.rar',
        #         'labels': {'restricted': False, 'starred': False, 'viewed': True, 'trashed': False, 'hidden': False},
        #         'mimeType': 'application/rar',
        #         'downloadUrl': 'https://doc-10-3s-docs.googleusercontent.com/docs/securesc/86pfcn16j2pqhe51bprtq4ltc8irqshs/3lk9bm295b40cjb7s406667n55qjcjs4/1497708000000/17510777207640926192/17510777207640926192/0BySf-c-YW0YAcUJ4SnhlYm5reFU?e=download&gd=true',
        #         'alternateLink': 'https://drive.google.com/file/d/0BySf-c-YW0YAcUJ4SnhlYm5reFU/view?usp=drivesdk',
        #         'etag': '"SDttlFaBtxNkII7l28jBkofy3p0/MTQ5NzYwOTcxNTA0Ng"', 'appDataContents': False,
        #         'webContentLink': 'https://drive.google.com/uc?id=0BySf-c-YW0YAcUJ4SnhlYm5reFU&export=download',
        #         'modifiedDate': '2017-06-16T10:41:55.046Z', 'spaces': ['drive'], 'userPermission': {'id': 'me',
        #                                                                                             'selfLink': 'https://www.googleapis.com/drive/v2/files/0BySf-c-YW0YAcUJ4SnhlYm5reFU/permissions/me',
        #                                                                                             'type': 'user',
        #                                                                                             'kind': 'drive#permission',
        #                                                                                             'etag': '"SDttlFaBtxNkII7l28jBkofy3p0/Wn8irBGiXoVjzDmwp-MUAzcZnYY"',
        #                                                                                             'role': 'owner'},
        #         'lastModifyingUser': {'isAuthenticatedUser': True, 'permissionId': '17510777207640926192',
        #                               'kind': 'drive#user', 'emailAddress': 'panaev02@gmail.com',
        #                               'displayName': 'Сергей Панаев'}, 'modifiedByMeDate': '2017-06-16T10:41:55.046Z',
        #         'shared': False, 'copyable': True, 'capabilities': {'canEdit': True, 'canCopy': True},
        #         'md5Checksum': '43d3d255b658bf7a57119a561d688019', 'lastViewedByMeDate': '2017-06-16T10:41:55.046Z',
        #         'fileExtension': 'rar', 'ownerNames': ['Сергей Панаев'], 'kind': 'drive#file',
        #         'iconLink': 'https://drive-thirdparty.googleusercontent.com/16/type/application/rar',
        #         'explicitlyTrashed': False}

        print('-----------------------------')
        self.list_view(file)
        # pprint.pprint(file)  #Работает, но форматирование мне не нравится
        print('-----------------------------')

    def copy_file(self, number, path=None):
        pass

    def delete_file(self, number):
        pass

    def trash_file(self, number):
        pass

    def untrash_file(self, number):
        pass

    def list_view(self, file, tabs=''):
        """ Сделать рефакторинг!
        Перенести в сервис.
        А лучше сделать гребанный фронт-контроллер!
        """
        if isinstance(file, dict):

            for key in file:

                if isinstance(file[key], int) or isinstance(file[key], str) or isinstance(file[key], bool):
                    print(tabs + key + " ===> " + str(file[key]))
                elif isinstance(file[key], list):
                    self.list_view(file[key], tabs + '\t')
                elif isinstance(file[key], dict):
                    self.list_view(file[key], tabs + '\t')

        elif isinstance(file, list):

            for item in file:

                if isinstance(item, int) or isinstance(item, str) or isinstance(item, bool):
                    print(tabs + str(item))
                else:
                    self.list_view(item, tabs + '\t')
