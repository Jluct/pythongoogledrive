# Example: loops monitoring events forever.
import datetime
import pyinotify
import os

path = '/home/serj/PyGoogleDrive'


class MyEventHandler(pyinotify.ProcessEvent):
    def my_init(self):
        """
        This is your constructor it is automatically called from
        ProcessEvent.__init__(), And extra arguments passed to __init__() would
        be delegated automatically to my_init().
        """
        self._file_object = open('/home/serj/log.txt', 'w+')

    def process_IN_DELETE(self, event):
        """
        This method processes a specific type of event: IN_DELETE. event
        is an instance of Event.
        """
        self._file_object.write('{} : deleting: {}\n'.format(self.get_dt(), event.pathname))
        print('{} : deleting: {}\n'.format(self.get_dt(), event.pathname))

    def process_IN_CLOSE(self, event):
        """
        This method is called on these events: IN_CLOSE_WRITE and
        IN_CLOSE_NOWRITE.
        """
        self._file_object.write('{} : write: {}\n'.format(self.get_dt(), event.pathname))
        print('{} : write: {}\n'.format(self.get_dt(), event.pathname))

    def process_IN_OPEN(self, event):
        self._file_object.write('{} : open: {}\n'.format(self.get_dt(), event.pathname))
        print('{} : open: {}\n'.format(self.get_dt(), event.pathname))

    def process_IN_MOVED_FROM(self, event):
        self._file_object.write('{} : moved from: {}\n'.format(self.get_dt(), event.pathname))
        print('{} : moved from: {}\n'.format(self.get_dt(), event.pathname))

    def process_IN_MOVED_TO(self, event):
        self._file_object.write('{} : moved to: {}\n'.format(self.get_dt(), event.pathname))
        print('{} : moved to: {}\n'.format(self.get_dt(), event.pathname))

    def process_default(self, event):
        """
        Eventually, this method is called for all others types of events.
        This method can be useful when an action fits all events.
        """
        print(event)
        self._file_object.write('{} : default: {}\n'.format(self.get_dt(), event.pathname))
        self._file_object.write(str(event))
        print('{} : default: {}\n'.format(self.get_dt(), event.pathname))

    def process_IN_MODIFY(self, event):
        self._file_object.write('{} : modify: {}\n'.format(self.get_dt(), event.pathname))
        print('{} : modify: {}\n'.format(self.get_dt(), event.pathname))

    def process_IN_CREATE(self, event):
        self._file_object.write('{} : create: {}\n'.format(self.get_dt(), event.pathname))
        print('{} : create: {}\n'.format(self.get_dt(), event.pathname))

    def proces_IN_ATTRIB(self, event):
        self._file_object.write('{} : attrib: {}\n'.format(self.get_dt(), event.pathname))
        print('{} : attrib: {}\n'.format(self.get_dt(), event.pathname))

    def close_file(self):
        self._file_object.close()

    def get_dt(self):
        return str(datetime.datetime.now().strftime('%H:%M:%S %d-%m-%Y'))


p = MyEventHandler()
# Instanciate a new WatchManager (will be used to store watches).
wm = pyinotify.WatchManager()
# Associate this WatchManager with a Notifier (will be used to report and
# process events).
notifier = pyinotify.Notifier(wm)
# Add a new watch on /tmp for ALL_EVENTS.
wm.add_watch(path, pyinotify.ALL_EVENTS, proc_fun=p)

for path, dirs, files in os.walk(path):
    for f in dirs:
        print(os.path.join(path, f))
        wm.add_watch(os.path.join(path, f), pyinotify.ALL_EVENTS, proc_fun=p)
        # Loop forever and handle events.

# wm.add_watch('/home/serj/PyGoogleDrive/test-wizard/test-wizard-1', pyinotify.ALL_EVENTS, proc_fun=p)

try:
    notifier.loop()
except Exception:
    print(Exception)
    p.close_file()
